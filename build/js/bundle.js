'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Inst = function () {
	function Inst(user, token) {
		_classCallCheck(this, Inst);

		this.user = user;
		this.token = token;
	}

	_createClass(Inst, [{
		key: 'sendResponse',
		value: function sendResponse(count) {
			var tagScript = document.createElement('script');
			tagScript.src = 'https://api.instagram.com/v1/users/' + this.user + '/media/recent?access_token=' + this.token + '&count=' + count + '&callback=Inst.responseData';
			document.head.appendChild(tagScript);
		}
	}], [{
		key: 'responseData',
		value: function responseData(data) {

			data.data.forEach(function (item) {
				Inst.addItem(item);
			});
		}
	}, {
		key: 'addItem',
		value: function addItem(obj) {
			var wrap = document.querySelector('.wrap');
			var item = document.createElement('article');

			console.log(obj);

			item.innerHTML = '\n\t\t\t<div class="item" data-id="' + obj.id + '">\n\t\t\t\t<header>\n\t\t\t\t\t<a class="item__avatar" href="https://www.instagram.com/' + obj.user.username + '/">\n\t\t\t\t\t\t<img src="' + obj.user.profile_picture + '" alt="' + obj.user.full_name + '">\n\t\t\t\t\t</a>\n\t\t\t\t\t<div class="item__author">\n\t\t\t\t\t\t<a class="item__username" href="https://www.instagram.com/' + obj.user.username + '/">' + obj.user.username + '</a>\n\t\t\t\t\t\t<span class="item__location">' + (obj.location ? obj.location.name : '') + '</span>\n\t\t\t\t\t</div>\n\t\t\t\t</header>\n\t\t\t\t<div class="item__photo">\n\t\t\t\t\t<img src="' + obj.images.standard_resolution.url + '">\n\t\t\t\t</div>\n\t\t\t\t<div class="item__body">\n\t\t\t\t\t<div class="item__line"></div>\n\t\t\t\t\t<div class="item__like">' + obj.likes.count + ' likes</div>\n\t\t\t\t\t<div class="item__text">' + (obj.caption ? obj.caption.text : '') + '</div>\n\t\t\t\t</div>\n\t\t\t</div>';

			wrap.appendChild(item);
		}
	}, {
		key: 'like',
		value: function like(id) {
			alert(id);
		}
	}]);

	return Inst;
}();

window.onload = initEvent;

function initEvent() {

	var wrap = document.querySelector('.wrap');

	wrap.onclick = function (event) {
		if (event.target.className == 'item__like') {
			var item = event.target.parentElement.parentElement;

			if (item.hasAttribute('data-id')) {
				Inst.like(item.getAttribute('data-id'));
			}
		}
	};
};

var test = new Inst(691623, '691623.1419b97.479e4603aff24de596b1bf18891729f3');
test.sendResponse(20);