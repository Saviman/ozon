class Inst {
	constructor( user, token ) {
		this.user = user;
		this.token = token;
	}

	sendResponse( count ) {
		let tagScript = document.createElement('script');
		tagScript.src = `https://api.instagram.com/v1/users/${this.user}/media/recent?access_token=${this.token}&count=${count}&callback=Inst.responseData`;
		document.head.appendChild( tagScript );
	}

	static responseData( data ) {

		data.data.forEach( ( item ) => {
			Inst.addItem( item );
		});

	}

	static addItem ( obj ) {
		let wrap = document.querySelector('.wrap');
		let item = document.createElement('article');

		console.log(obj);

		item.innerHTML = `
			<div class="item" data-id="${obj.id}">
				<header>
					<a class="item__avatar" href="https://www.instagram.com/${obj.user.username}/">
						<img src="${obj.user.profile_picture}" alt="${obj.user.full_name}">
					</a>
					<div class="item__author">
						<a class="item__username" href="https://www.instagram.com/${obj.user.username}/">${obj.user.username}</a>
						<span class="item__location">${ obj.location ? obj.location.name : ''  }</span>
					</div>
				</header>
				<div class="item__photo">
					<img src="${obj.images.standard_resolution.url}">
				</div>
				<div class="item__body">
					<div class="item__line"></div>
					<div class="item__like">${obj.likes.count} likes</div>
					<div class="item__text">${ obj.caption ? obj.caption.text : ''}</div>
				</div>
			</div>`;

		wrap.appendChild( item );
	}
	static like( id ) {
		alert( id );
	}
}

window.onload = initEvent;

function initEvent() {

	let wrap = document.querySelector('.wrap');

	wrap.onclick = ( event ) => {
		if ( event.target.className == 'item__like' ) {
			let item = event.target.parentElement.parentElement;

			if ( item.hasAttribute('data-id') ) {
				Inst.like( item.getAttribute('data-id') );
			}
		}
	};

};


let test = new Inst( 691623, '691623.1419b97.479e4603aff24de596b1bf18891729f3');
test.sendResponse(20);
